﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO; // used to write files
using System.Threading; // to pause


namespace passwordGenerator
{
    class Program
    {
        public static char[] GenerateCharactersArray(char userArrayChoice1, char userArrayChoice2, char userArrayChoice3)
        {
            char[] alphabetCharacters = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'w', 'x', 'y', 'z' }; //basic alphabet characters
            char[] polishCharacters = { 'ą', 'ć', 'ę', 'ł', 'ń', 'ó', 'ś', 'ź', 'ż' }; // polish characters
            char[] specialCharacters = { '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '-', '_', '=', '+', '`', '"', ';', ':', '[', ']', '{', '}' }; // special characters

            char[] combinedCharacters = alphabetCharacters;
            if (userArrayChoice1 == 'y')
            {
                combinedCharacters = combinedCharacters.Concat(polishCharacters).ToArray(); // adding polish characters to basic characters
            }
            if (userArrayChoice2 == 'y')
            {
                combinedCharacters = combinedCharacters.Concat(specialCharacters).ToArray(); // adding special characters to basic characters
            }
            char[] upperCharacters = new char[combinedCharacters.Length];
            if (userArrayChoice3 == 'y') // if user wants uppercase chars 
            {
                int indeks;
                foreach (char ch in combinedCharacters) // this one adds uppercase to all chars that are wanted by user
                {
                    indeks = Array.IndexOf(combinedCharacters, ch);
                    upperCharacters[indeks] = char.ToUpper(ch);
                }

                combinedCharacters = combinedCharacters.Concat(upperCharacters).ToArray();
            }

            return combinedCharacters;
        }

        public static char[] RandomizeArray(char[] combinedArray) // randomize combinedCharacters array to make it more random
        {
            char[] randomizedArray = new char[combinedArray.Length];
            Random randomNum = new Random();

            for (int i = 0; i < combinedArray.Length; i++)
            {
                int randomIndex = randomNum.Next(combinedArray.Length);
                randomizedArray[i] = combinedArray[randomIndex];
            }

            return randomizedArray;
        }

        public static string GenerateRandomString(int amountOfChars, char userArrayChoice1, char userArrayChoice2, char userArrayChoice3)
        {
            char[] combinedArray;
            combinedArray = GenerateCharactersArray(userArrayChoice1, userArrayChoice2, userArrayChoice3);
            char[] randomizedArray;
            randomizedArray = RandomizeArray(combinedArray); // randomize array
            char[] finalArray = randomizedArray; // final array to generate pass

            string newPass = "";
            Random rnd = new Random();

            while (amountOfChars > 0)
            {
                int numberFromArray = rnd.Next(finalArray.Length); // choose random number 
                string newChar = (finalArray[numberFromArray]).ToString(); // get character aligned to this number
                newPass = newPass + newChar;
                amountOfChars--;
            }

            return newPass;
        }

        public static bool CheckIfExist(string newString) // checks if current password exist in file; still generate password but dont save it
        {
            bool existOrNot = false;

            string path = @"C:\Users\Oskar\Documents\C#\passwordGenerator\passwordGenerator\passwordGenerator\passwords.txt";

            foreach (var line in File.ReadAllLines(path))
            {
                if (line.Contains(newString))
                {
                    existOrNot = true;
                }
            }
            return existOrNot;
        }

        public static void SaveToFile(string newString)
        {
            string path = @"C:\Users\Oskar\Documents\C#\passwordGenerator\passwordGenerator\passwordGenerator\passwords.txt";

            if (!File.Exists(path)) // create file if not exist
            {
                File.WriteAllText(path, "a");
            }
            bool passExists = CheckIfExist(newString); // checks if password exists in file
            if (new FileInfo(path).Length != 0 && passExists != true) // checks if there are any old passwords in file. if yes: makes new line and then adds current pass
            {
                File.AppendAllText(path, Environment.NewLine);
                File.AppendAllText(path, newString);
            }
            else // if not adds only new pass
            {
                Console.WriteLine("Takie haslo istnieje");                
            }
        }

        public static void ReadFromFile()
        {
            string path = @"C:\Users\Oskar\Documents\C#\passwordGenerator\passwordGenerator\passwordGenerator\passwords.txt";

            string readText = File.ReadAllText(path); // reads all passwords from file
            Console.WriteLine("Zapisane hasła: ");
            Console.WriteLine(readText);
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Ile hasel wygenerowac?");
            int howManyPass = int.Parse(Console.ReadLine());

            Console.WriteLine("Jak dlugie ma byc haslo?");
            int amountOfChars = int.Parse(Console.ReadLine());

            Console.WriteLine("Czy chcesz uzyc polskich znakow? y/n");
            char userArrayChoice1 = char.Parse(Console.ReadLine());
            Console.WriteLine("Czy chcesz uzyc znakow specjalnych? y/n");
            char userArrayChoice2 = char.Parse(Console.ReadLine());
            Console.WriteLine("Czy chcesz uzyc wielkich liter? y/n");
            char userArrayChoice3 = char.Parse(Console.ReadLine());

            for(int i = 0; i < howManyPass; i++)
            {
                string newString = GenerateRandomString(amountOfChars, userArrayChoice1, userArrayChoice2, userArrayChoice3);
                Console.WriteLine("Twoje hasło: " + newString);
                SaveToFile(newString);
                Thread.Sleep(5);
            }

            ReadFromFile();
            

            Console.ReadLine();

        }
    }
}
